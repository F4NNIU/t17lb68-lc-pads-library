#T17LB68-LC-PADS-Library
# PADS PCB 封装库

版本 V3.2.0。

使用 git 方便版本控制。

适用软件：PADS 9.5

> * 升级 LC-VSSOP-10 版本为 V2，修正引脚编号，信息来自 AMOBBS。
> * 升级 LC-SOIC-14_300mil 版本为 V2，修正引脚编号，信息来自 AMOBBS。
> * 升级 LC-TQFN-16_4x4x065P 版本为 V2，修正引脚编号，信息来自 AMOBBS。
> * 升级 LC-TQFP-128_14x14x04P 版本为 V2，修正引脚编号，信息来自 AMOBBS。
> * 升级 LC-DFN-8_3x3mm 版本为 V2，修正引脚编号，信息来自 AMOBBS。
> * 升级 LC-SC-70(SC-70-3) 版本为 V2，修正引脚编号，信息来自 F4NNIU。
> * 升级 LC-USM(SC-70-3) 版本为 V2，修正引脚编号，信息来自 F4NNIU。
> * 升级 LC-PQFP-160_28X28X065P 版本为 V2，修正引脚编号，信息来自 F4NNIU。
> * 升级 LC-SMF-045X 版本为 V2，修正引脚编号，信息来自 F4NNIU。

原始文件来源：http://www.amobbs.com/thread-5656023-1-1.html
