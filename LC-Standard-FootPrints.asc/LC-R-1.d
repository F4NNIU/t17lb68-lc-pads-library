*PADS-LIBRARY-PCB-DECALS-V9*

LC-R-1           I 0     0     2 2 8 0 2 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-200.85 63.12 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-200.85 113.12 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 7.87 26 -1
-78.74 51.18
78.74 51.18
78.74 -51.18
-78.74 -51.18
-78.74 51.18
OPEN   3 7.87 26 -1
-78.74 51.18
-55.12 51.18
-55.12 -51.18
OPEN   2 7.87 26 -1
-102.36 0    
-78.74 0    
OPEN   2 7.87 26 -1
78.74 0    
102.36 0    
OPEN   2 7.87 26 -1
-47.24 -51.18
-47.24 51.18
OPEN   2 8 19 -1
216.54 -19.68
216.54 19.68
OPEN   2 8 19 -1
196.85 0    
236.22 0    
OPEN   2 8 19 -1
-196.85 -19.68
-196.85 19.68
T150   0     150   0     1
T-150  0     -150  0     2
PAD 1 3 P 35.43
-2 62.99 S   0  
-1 62.99 S   0  
0  62.99 S   0  
PAD 2 3 P 35.43
-2 62.99 R  
-1 62.99 R  
0  62.99 R  

*END*
