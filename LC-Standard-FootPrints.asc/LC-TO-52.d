*PADS-LIBRARY-PCB-DECALS-V9*

LC-TO-52         I 0     0     2 2 4 0 3 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-136.5 127.96 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-136.5 177.96 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 10 26 -1
114.96 0    
-114.96 0    
CIRCLE 2 10 18 -1
114.96 0    
-114.96 0    
OPEN   4 10 26 -1
-96.06 -62.99
-131.5 -98.03
-98.03 -131.5
-62.99 -96.06
OPEN   4 10 18 -1
-96.06 -62.99
-131.5 -98.03
-98.03 -131.5
-62.99 -96.06
T0     -50   0     -50   1
T50    0     50    0     2
T0     50    0     50    3
PAD 0 5 P 27.56
-2 47.24 R  
-1 47.24 R  
0  47.24 R  
21 51.24 R  
28 51.24 R  
PAD 3 5 P 27.56
-2 47.24 RF  0   90   47.24 0  
-1 47.24 RF  0   90   47.24 0  
0  47.24 RF  0   90   47.24 0  
21 51.24 RF  0   90   51.24 0  
28 51.24 RF  0   90   51.24 0  

*END*
