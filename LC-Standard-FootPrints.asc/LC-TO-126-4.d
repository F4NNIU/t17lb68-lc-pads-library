*PADS-LIBRARY-PCB-DECALS-V9*

LC-TO-126-4      I 0     0     2 2 4 0 4 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-204.72 103.55 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-204.72 153.55 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 15.75 26 -1
-181.1 0    
-196.85 0    
CLOSED 5 7.87 18 -1
-161.42 86.61
161.42 86.61
161.42 -47.24
-161.42 -47.24
-161.42 86.61
CLOSED 5 10 26 -1
-165.35 90.55
165.35 90.55
165.35 -51.18
-165.35 -51.18
-165.35 90.55
OPEN   2 10 26 -1
-165.35 74.8 
165.35 74.8 
T-105.12 0     -105.12 0     1
T-35.04 0     -35.04 0     2
T35.04 0     35.04 0     3
T105.12 0     105.12 0     4
PAD 1 5 P 43.31
-2 59.06 RF  0   90   59.06 0  
-1 59.06 RF  0   90   59.06 0  
0  59.06 RF  0   90   59.06 0  
21 63.06 RF  0   90   63.06 0  
28 63.06 RF  0   90   63.06 0  
PAD 0 5 P 43.31
-2 59.06 R  
-1 59.06 R  
0  59.06 R  
21 63.06 R  
28 63.06 R  

*END*
