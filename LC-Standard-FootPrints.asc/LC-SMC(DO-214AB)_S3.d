*PADS-LIBRARY-PCB-DECALS-V9*

LC-SMC(DO-214AB)_S3 I 0     0     2 2 6 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-216.6 124.93 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-216.6 174.93 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 3.94 18 -1
135.04 -114.96
135.04 114.96
-135.04 114.96
-135.04 -114.96
135.04 -114.96
OPEN   4 10 26 -1
-39.37 -106.3
-39.37 106.3
-47.24 106.3
-47.24 -106.3
CLOSED 5 10 26 -1
-70.87 -106.3
66.93 -106.3
66.93 106.3
-70.87 106.3
-70.87 -106.3
OPEN   2 8 19 -1
208.66 0    
248.03 0    
OPEN   2 8 19 -1
228.35 -19.68
228.35 19.68
OPEN   2 8 19 -1
-212.6 -19.68
-212.6 19.68
T140.49 0     140.49 0     1
T-140.49 0     -140.49 0     2
PAD 0 3 P 0    
-2 110 RF  0   90   150 0  
-1 0   R  
0  0   R  

*END*
