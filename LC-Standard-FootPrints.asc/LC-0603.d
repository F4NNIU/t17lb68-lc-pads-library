*PADS-LIBRARY-PCB-DECALS-V9*

LC-0603          I 0     0     2 2 1 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-48.62 27.29 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-48.62 77.29 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 1.97 18 -1
-31.5 15.75
31.5  15.75
31.5  -15.75
-31.5 -15.75
-31.5 15.75
T-30.31 0     -30.31 0     1
T30.31 0     30.31 0     2
PAD 0 3 P 0    
-2 36.61 RF  0   90   38.58 0  
-1 0   R  
0  0   R  

*END*
