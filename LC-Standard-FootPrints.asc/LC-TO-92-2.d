*PADS-LIBRARY-PCB-DECALS-V9*

LC-TO-92-2       I 0     0     2 2 3 0 2 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-94.29 105.32 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-94.29 155.32 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
OPEN   3 3.94 18 -1
-75.56 -53.05 2150 -2501 -92.32 -92.32 92.32 92.32
75.58 -53.01
-75.55 -53.06
OPEN   2 10 26 -1
82.58 41.29 265 1268 -92.32 -92.32 92.32 92.32
-82.58 41.29
OPEN   2 10 26 -1
-75.55 -53.07
75.55 -53.07
T-50   0     -50   0     1
T50    0     50    0     2
PAD 1 3 P 31.5 
-2 53.15 S   0  
-1 53.15 S   0  
0  53.15 S   0  
PAD 2 3 P 31.5 
-2 53.15 R  
-1 53.15 R  
0  53.15 R  

*END*
