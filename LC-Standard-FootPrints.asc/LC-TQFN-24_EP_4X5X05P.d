*PADS-LIBRARY-PCB-DECALS-V9*

LC-TQFN-24_EP_4X5X05P I 0     0     2 2 7 0 25 19 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-114.2 102.52 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-114.2 152.52 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 11.81 26 -1
-92.52 -106.3
-104.33 -106.3
CIRCLE 2 3.94 18 -1
-66.93 -59.06
-90.55 -59.06
CLOSED 5 3.94 18 -1
-98.43 -78.74
-98.43 78.74
98.43 78.74
98.43 -78.74
-98.43 -78.74
OPEN   3 7.87 26 -1
-78.74 86.61
-106.3 86.61
-106.3 57.09
OPEN   3 7.87 26 -1
106.3 -57.09
106.3 -86.61
78.74 -86.61
OPEN   3 7.87 26 -1
106.3 57.09
106.3 86.61
78.74 86.61
OPEN   3 7.87 26 -1
-78.74 -86.61
-106.3 -86.61
-106.3 -57.09
T0     0     0     0     0
T-59.06 -76.77 -59.06 -76.77 1
T-39.37 -76.77 -39.37 -76.77 2
T-19.68 -76.77 -19.68 -76.77 3
T0     -76.77 0     -76.77 4
T19.69 -76.77 19.69 -76.77 5
T39.37 -76.77 39.37 -76.77 6
T59.06 -76.77 59.06 -76.77 7
T96.46 -39.37 96.46 -39.37 8
T96.46 -19.69 96.46 -19.69 9
T96.46 0     96.46 0     10
T96.46 19.68 96.46 19.68 11
T96.46 39.37 96.46 39.37 12
T59.06 76.77 59.06 76.77 13
T39.37 76.77 39.37 76.77 14
T19.68 76.77 19.68 76.77 15
T0     76.77 0     76.77 16
T-19.69 76.77 -19.69 76.77 17
T-39.37 76.77 -39.37 76.77 18
T-59.06 76.77 -59.06 76.77 19
T-96.46 39.37 -96.46 39.37 20
T-96.46 19.69 -96.46 19.69 21
T-96.46 0     -96.46 0     22
T-96.46 -19.68 -96.46 -19.68 23
T-96.46 -39.37 -96.46 -39.37 24
PAD 1 4 P 0    
-2 102.36 RF  0   0    141.73 0  
-1 0   R  
0  0   R  
21 106.36 RF  0   0    145.73 0  
PAD 0 4 P 0    
-2 11.02 OF  90   31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  90   35.5 0  
PAD 9 4 P 0    
-2 11.02 OF  0    31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    35.5 0  
PAD 10 4 P 0    
-2 11.02 OF  0    31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    35.5 0  
PAD 11 4 P 0    
-2 11.02 OF  0    31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    35.5 0  
PAD 12 4 P 0    
-2 11.02 OF  0    31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    35.5 0  
PAD 13 4 P 0    
-2 11.02 OF  0    31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    35.5 0  
PAD 14 4 P 0    
-2 11.02 OF  90   31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  90   35.5 0  
PAD 15 4 P 0    
-2 11.02 OF  90   31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  90   35.5 0  
PAD 16 4 P 0    
-2 11.02 OF  90   31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  90   35.5 0  
PAD 17 4 P 0    
-2 11.02 OF  90   31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  90   35.5 0  
PAD 18 4 P 0    
-2 11.02 OF  90   31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  90   35.5 0  
PAD 19 4 P 0    
-2 11.02 OF  90   31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  90   35.5 0  
PAD 20 4 P 0    
-2 11.02 OF  90   31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  90   35.5 0  
PAD 21 4 P 0    
-2 11.02 OF  0    31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    35.5 0  
PAD 22 4 P 0    
-2 11.02 OF  0    31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    35.5 0  
PAD 23 4 P 0    
-2 11.02 OF  0    31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    35.5 0  
PAD 24 4 P 0    
-2 11.02 OF  0    31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    35.5 0  
PAD 25 4 P 0    
-2 11.02 OF  0    31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    35.5 0  

*END*
