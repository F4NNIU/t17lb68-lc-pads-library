*PADS-LIBRARY-PCB-DECALS-V9*

LC-SOP-8_EP_150MIL I 0     0     2 2 4 0 9 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-118.11 141.86 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-118.11 191.86 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 11.81 26 -1
-100.39 -114.17
-112.2 -114.17
CLOSED 5 3.94 18 -1
-99.41 -78.54
99.41 -78.54
99.41 78.54
-99.41 78.54
-99.41 -78.54
OPEN   2 10 26 -1
94.49 -62.99
94.49 59.06
OPEN   2 10 26 -1
-94.49 -62.99
-94.49 59.06
T0     0     0     0     0
T-75   -102.36 -75   -102.36 1
T-25   -102.36 -25   -102.36 2
T25    -102.36 25    -102.36 3
T75    -102.36 75    -102.36 4
T75    102.36 75    102.36 5
T25    102.36 25    102.36 6
T-25   102.36 -25   102.36 7
T-75   102.36 -75   102.36 8
PAD 1 3 P 0    
-2 114.17 RF  0   0    157.48 0  
-1 0   R  
0  0   R  
PAD 0 3 P 0    
-2 23.62 OF  90   62.99 0  
-1 0   R  
0  0   R  

*END*
