*PADS-LIBRARY-PCB-DECALS-V9*

LC-TO-220F-4L(FORMING) I 0     0     2 2 4 0 4 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-205.79 79.93 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-205.79 129.93 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 15.75 26 -1
-145.67 -62.99
-161.42 -62.99
OPEN   2 10 26 -1
-200.79 51.18
200.79 51.18
OPEN   5 10 26 -1
200.79 -98.43
200.79 66.93
-200.79 66.93
-200.79 -118.11
-106.3 -118.11
OPEN   2 10 26 -1
7.87  -118.11
90.55 -118.11
T-150  0     -150  0     1
T-50   -125.2 -50   -125.2 2
T50    0     50    0     3
T150   -125.2 150   -125.2 4
PAD 1 3 P 47.24
-2 78.74 S   0  
-1 78.74 S   0  
0  78.74 S   0  
PAD 0 3 P 47.24
-2 78.74 R  
-1 78.74 R  
0  78.74 R  

*END*
