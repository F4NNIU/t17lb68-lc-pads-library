*PADS-LIBRARY-PCB-DECALS-V9*

LC-SOT-223       I 0     0     2 2 2 0 4 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-161.42 141.86 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-161.42 191.86 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 3.94 18 -1
72.83 -131.89
72.83 131.89
-72.83 131.89
-72.83 -131.89
72.83 -131.89
CLOSED 5 10 26 -1
55.12 -118.11
55.12 118.11
-55.12 118.11
-55.12 -118.11
55.12 -118.11
T118.11 -90.55 118.11 -90.55 1
T118.11 0     118.11 0     2
T-118.11 0     -118.11 0     2/2
T118.11 90.55 118.11 90.55 3
PAD 0 3 P 0    
-2 39.37 RF  0   0    86.61 0  
-1 0   R  
0  0   R  
PAD 3 3 P 0    
-2 86.61 RF  0   90   137.8 0  
-1 0   R  
0  0   R  

*END*
