*PADS-LIBRARY-PCB-DECALS-V9*

LC-SOIC-14_150MIL I 0     0     2 2 5 0 14 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-192.91 145.8 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-192.91 195.8 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 15.75 26 -1
-129.92 -27.56
-145.67 -27.56
CIRCLE 2 11.81 26 -1
-175.2 -118.11
-187.01 -118.11
CIRCLE 2 3.94 18 -1
-117.13 -43.31
-156.5 -43.31
CLOSED 5 10 26 -1
165.35 -55.12
165.35 55.12
-165.35 55.12
-165.35 -55.12
165.35 -55.12
CLOSED 5 3.94 18 -1
172.24 -78.74
172.24 78.74
-172.24 78.74
-172.24 -78.74
172.24 -78.74
T-150  -102.36 -150  -102.36 1
T-100  -102.36 -100  -102.36 2
T-50   -102.36 -50   -102.36 3
T0     -102.36 0     -102.36 4
T50    -102.36 50    -102.36 5
T100   -102.36 100   -102.36 6
T150   -102.36 150   -102.36 7
T150   102.36 150   102.36 8
T100   102.36 100   102.36 9
T50    102.36 50    102.36 10
T0     102.36 0     102.36 11
T-50   102.36 -50   102.36 12
T-100  102.36 -100  102.36 13
T-150  102.36 -150  102.36 14
PAD 0 3 P 0    
-2 23.62 OF  90   70.87 0  
-1 0   R  
0  0   R  

*END*
