*PADS-LIBRARY-PCB-DECALS-V9*

LC-SOT-143       I 0     0     2 2 2 0 4 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-82.95 75.99 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-82.95 125.99 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 11.81 26 -1
72.05 -86.61
60.24 -86.61
CLOSED 5 10 26 -1
-77.95 -62.99
-77.95 62.99
77.95 62.99
77.95 -62.99
-77.95 -62.99
T39.37 -27.95 39.37 -27.95 1
T37.8  37.4  37.8  37.4  2
T-39.37 37.4  -39.37 37.4  3
T-39.37 -37.4 -39.37 -37.4 4
PAD 1 3 P 0    
-2 43.31 RF  0   0    51.18 0  
-1 0   R  
0  0   R  
PAD 0 3 P 0    
-2 23.62 OF  0    51.18 0  
-1 0   R  
0  0   R  

*END*
