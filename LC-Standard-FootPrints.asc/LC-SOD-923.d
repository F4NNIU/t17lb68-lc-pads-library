*PADS-LIBRARY-PCB-DECALS-V9*

LC-SOD-923       I 0     0     2 2 6 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-57.09 31.62 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-57.09 81.62 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 1.97 18 -1
-15.75 11.81
15.75 11.81
15.75 -11.81
-15.75 -11.81
-15.75 11.81
OPEN   2 3.94 19 -1
-55.12 -11.81
-55.12 11.81
OPEN   2 3.94 19 -1
55.12 -11.81
55.12 11.81
OPEN   2 3.94 19 -1
43.31 0    
66.93 0    
OPEN   2 7.87 26 -1
-47.24 -11.81
-47.24 11.81
CLOSED 5 7.87 26 -1
-35.43 -19.68
-35.43 19.68
35.43 19.68
35.43 -19.68
-35.43 -19.68
T17.72 0     17.72 0     1
T-17.72 0     -17.72 0     2
PAD 0 3 P 0    
-2 15.75 RF  0   90   19.68 0  
-1 0   R  
0  0   R  

*END*
