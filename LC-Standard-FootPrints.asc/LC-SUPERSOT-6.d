*PADS-LIBRARY-PCB-DECALS-V9*

LC-SUPERSOT-6    I 0     0     2 2 2 0 6 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-70.47 72.06 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-70.47 122.06 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 11.81 26 -1
64.96 -70.87
53.15 -70.87
CLOSED 5 10 26 -1
-15.75 59.06
15.75 59.06
15.75 -59.05
-15.75 -59.05
-15.75 59.06
T50.98 -37.4 50.98 -37.4 1
T50.98 0     50.98 0     2
T50.98 37.4  50.98 37.4  3
T-50.98 37.4  -50.98 37.4  4
T-50.98 0     -50.98 0     5
T-50.98 -37.4 -50.98 -37.4 6
PAD 0 3 P 0    
-2 23.62 RF  0   0    38.98 0  
-1 0   R  
0  0   R  

*END*
