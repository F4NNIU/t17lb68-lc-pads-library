*PADS-LIBRARY-PCB-DECALS-V9*

LC-TO-274AA(SUPER-247) I 0     0     2 2 4 0 3 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-323.9 127.17 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-323.9 177.17 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
OPEN   2 10 26 -1
-318.9 98.43
315.94 98.43
CLOSED 5 10 26 -1
-318.9 114.17
318.9 114.17
318.9 -94.49
-318.9 -94.49
-318.9 114.17
OPEN   3 10 26 -1
318.9 -72.83
288.39 -72.83
288.39 -94.49
OPEN   3 10 26 -1
-286.42 -92.52
-286.42 -73.82
-316.93 -73.82
T-214.57 0     -214.57 0     1
T0     0     0     0     2
T214.57 0     214.57 0     3
PAD 1 3 P 66.93
-2 118.11 S   0  
-1 118.11 S   0  
0  118.11 S   0  
PAD 0 3 P 66.93
-2 118.11 R  
-1 118.11 R  
0  118.11 R  

*END*
