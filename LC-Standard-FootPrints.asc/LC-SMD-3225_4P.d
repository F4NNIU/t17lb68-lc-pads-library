*PADS-LIBRARY-PCB-DECALS-V9*

LC-SMD-3225_4P   I 0     0     2 2 2 0 4 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-86.68 78.93 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-86.68 128.93 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 11.81 26 -1
-68.9 -86.61
-80.71 -86.61
CLOSED 5 8 26 -1
-82.68 -66.93
82.68 -66.93
82.68 66.93
-82.68 66.93
-82.68 -66.93
T-43.31 -31.5 -43.31 -31.5 1
T43.31 -31.5 43.31 -31.5 2
T43.31 31.5  43.31 31.5  3
T-43.31 31.5  -43.31 31.5  4
PAD 0 3 P 0    
-2 45.28 RF  0   0    55.12 0  
-1 0   R  
0  0   R  

*END*
