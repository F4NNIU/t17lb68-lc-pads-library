*PADS-LIBRARY-PCB-DECALS-V9*

LC-SMAF_S4       I 0     0     2 2 7 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-173.29 126.98 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
-173.29 76.98 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
CLOSED 5 3.94 18 -1
-74.8 55.12
74.8  55.12
74.8  -55.12
-74.8 -55.12
-74.8 55.12
OPEN   4 10 26 -1
-39.37 -62.99
-130.91 -62.99
-130.91 63.98
-39.37 63.98
OPEN   4 10 26 -1
39.37 63.98
130.91 63.98
130.91 -62.99
39.37 -62.99
OPEN   2 8 19 -1
-169.29 -19.68
-169.29 19.68
OPEN   2 8 19 -1
165.35 -19.68
165.35 19.68
OPEN   2 8 19 -1
145.67 0    
185.04 0    
COPCLS 5 10 26 -1
-158.31 -43.31
-142.56 -43.31
-142.56 43.31
-158.31 43.31
-158.31 -43.31
T78.74 0     78.74 0     1
T-78.74 0     -78.74 0     2
PAD 0 3 P 0    
-2 70.87 RF  0   0    78.74 0  
-1 0   R  
0  0   R  

*END*
