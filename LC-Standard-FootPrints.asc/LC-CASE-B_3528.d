*PADS-LIBRARY-PCB-DECALS-V9*

LC-CASE-B_3528   I 0     0     2 2 6 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-129  77.96 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-129  127.96 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
OPEN   6 10 26 -1
31.5  64.96
107.36 64.96
124.53 47.24
124.53 -47.24
107.36 -64.96
31.5  -64.96
OPEN   4 10 26 -1
-31.5 -64.96
-107.36 -64.96
-107.36 64.96
-31.5 64.96
OPEN   2 10 26 -1
107.36 -64.96
107.36 64.96
OPEN   2 8 19 -1
157.48 -19.68
157.48 19.68
OPEN   2 8 19 -1
137.8 0    
177.17 0    
OPEN   2 8 19 -1
-125  -20.67
-125  18.7 
T59.06 0     59.06 0     1
T-59.05 0     -59.05 0     2
PAD 0 3 P 0    
-2 70.87 RF  0   90   102.36 0  
-1 0   R  
0  0   R  

*END*
