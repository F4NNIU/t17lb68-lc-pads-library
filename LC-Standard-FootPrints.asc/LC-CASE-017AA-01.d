*PADS-LIBRARY-PCB-DECALS-V9*

LC-CASE-017AA-01 I 0     0     2 2 7 0 2 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-397.7 83.87 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-397.7 133.87 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
OPEN   2 8 26 -1
161.42 0    
253.94 0    
OPEN   2 8 26 -1
-248.03 0    
-155.51 0    
CLOSED 5 10 26 -1
159.45 -70.87
159.45 70.87
-155.51 70.87
-155.51 -70.87
159.45 -70.87
OPEN   4 10 26 -1
-116.14 70.87
-116.14 -70.87
-125.98 -70.87
-125.98 70.87
OPEN   2 8 19 -1
-393.7 -19.68
-393.7 19.68
OPEN   2 8 19 -1
393.7 0    
433.07 0    
OPEN   2 8 19 -1
413.39 -19.68
413.39 19.68
T322.83 0     322.83 0     1
T-322.83 0     -322.83 0     2
PAD 1 3 P 51.18
-2 98.43 S   0  
-1 98.43 S   0  
0  98.43 S   0  
PAD 2 3 P 51.18
-2 98.43 R  
-1 98.43 R  
0  98.43 R  

*END*
