*PADS-LIBRARY-PCB-DECALS-V9*

LC-SOD-882       I 0     0     2 2 3 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-23.65 37.53 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-23.65 87.53 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 1.97 18 -1
-19.68 -11.81
19.69 -11.81
19.69 11.81
-19.68 11.81
-19.68 -11.81
OPEN   2 7.87 26 -1
-15.75 25.59
15.75 25.59
OPEN   2 7.87 26 -1
-15.75 -25.59
15.75 -25.59
T13.78 0     13.78 0     1
T-13.78 0     -13.78 0     2
PAD 0 4 P 0    
-2 15.75 RF  0   90   27.56 0  
-1 0   R  
0  0   R  
21 19.75 RF  0   90   31.56 0  

*END*
