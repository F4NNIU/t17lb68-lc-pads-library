*PADS-LIBRARY-PCB-DECALS-V9*

LC-SMD4532       I 0     0     2 2 3 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-118.11 91.74 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-118.11 141.74 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
OPEN   2 10 26 -1
-43.31 -78.74
43.31 -78.74
OPEN   2 10 26 -1
-43.31 78.74
43.31 78.74
CLOSED 5 3.94 18 -1
-94.49 -70.87
-94.49 70.87
94.49 70.87
94.49 -70.87
-94.49 -70.87
T-88.58 0     -88.58 0     1
T88.58 0     88.58 0     2
PAD 0 3 P 0    
-2 59.06 RF  0   90   165.35 0  
-1 0   R  
0  0   R  

*END*
