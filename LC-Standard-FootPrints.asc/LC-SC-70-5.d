*PADS-LIBRARY-PCB-DECALS-V9*

LC-SC-70-5       I 0     0     2 2 2 0 5 3 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-59.87 55.24 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-59.87 105.24 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 7.87 18 -1
-27.56 -43.31
27.56 -43.31
27.56 43.31
-27.56 43.31
-27.56 -43.31
CLOSED 5 7.87 26 -1
7.87  -39.37
7.87  39.37
-7.87 39.37
-7.87 -39.37
7.87  -39.37
T38.19 -25.59 38.19 -25.59 1
T38.19 0     38.19 0     2
T38.19 25.59 38.19 25.59 3
T-38.19 25.59 -38.19 25.59 4
T-38.19 -25.59 -38.19 -25.59 5
PAD 0 4 P 0    
-2 16.54 RF  0   0    39.37 0  
-1 0   R  
0  0   R  
21 20.54 RF  0   0    43.37 0  
PAD 4 4 P 0    
-2 16.54 RF  0   0    39.37 0  
-1 0   R  
0  0   R  
21 20.54 RF  0   0    43.37 0  
PAD 5 4 P 0    
-2 16.54 RF  0   0    39.37 0  
-1 0   R  
0  0   R  
21 20.54 RF  0   0    43.37 0  

*END*
