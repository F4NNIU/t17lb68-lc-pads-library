*PADS-LIBRARY-PCB-DECALS-V9*

LC-CASE-D_7343   I 0     0     2 2 6 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-204.79 111.43 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-204.79 161.43 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
OPEN   2 10 26 -1
182.95 -98.43
182.95 98.43
OPEN   4 10 26 -1
-78.74 -98.43
-182.95 -98.43
-182.95 98.43
-78.74 98.43
OPEN   6 10 26 -1
78.74 -98.43
182.95 -98.43
200.79 -74.8
200.79 74.8 
182.95 98.43
78.74 98.43
OPEN   2 8 19 -1
-200.79 -19.68
-200.79 19.68
OPEN   2 8 19 -1
216.54 0    
255.91 0    
OPEN   2 8 19 -1
236.22 -19.68
236.22 19.68
T122.83 0     122.83 0     1
T-122.83 0     -122.83 0     2
PAD 0 3 P 0    
-2 94.49 RF  0   90   118.11 0  
-1 0   R  
0  0   R  

*END*
