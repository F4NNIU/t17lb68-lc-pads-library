*PADS-LIBRARY-PCB-DECALS-V9*

LC-DO-35         I 0     0     2 2 7 0 2 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-200.85 49.34 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-200.85 99.34 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
OPEN   2 7.87 26 -1
75.23 0    
102.36 0    
OPEN   2 7.87 26 -1
-102.36 0    
-78.77 0    
CLOSED 5 7.87 26 -1
-78.77 -37.4
-78.77 37.4 
75.23 37.4 
74.77 -37.4
-78.77 -37.4
OPEN   4 7.87 26 -1
-57.01 -37.4
-57.01 37.4 
-51.18 37.4 
-51.18 -37.4
OPEN   2 8 19 -1
212.6 -19.68
212.6 19.68
OPEN   2 8 19 -1
192.91 0    
232.28 0    
OPEN   2 8 19 -1
-196.85 -19.68
-196.85 19.68
T150   0     150   0     1
T-150  0     -150  0     2
PAD 1 3 P 31.5 
-2 62.99 S   0  
-1 62.99 S   0  
0  62.99 S   0  
PAD 2 3 P 31.5 
-2 62.99 R  
-1 62.99 R  
0  62.99 R  

*END*
