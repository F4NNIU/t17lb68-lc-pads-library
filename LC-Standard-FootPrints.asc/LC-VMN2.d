*PADS-LIBRARY-PCB-DECALS-V9*

LC-VMN2          I 0     0     2 2 6 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-57.94 34.57 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-57.94 84.57 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 3.94 18 -1
-17.72 -11.81
-17.72 11.81
17.72 11.81
17.72 -11.81
-17.72 -11.81
OPEN   2 7.87 26 -1
-54   -15.75
-54   15.75
CLOSED 5 7.87 26 -1
-43.31 -22.64
43.31 -22.64
43.31 22.64
-43.31 22.64
-43.31 -22.64
OPEN   2 3.94 19 -1
-35.43 -11.81
-35.43 11.81
OPEN   2 3.94 19 -1
62.99 -11.81
62.99 11.81
OPEN   2 3.94 19 -1
51.18 0    
74.8  0    
T19.68 0     19.68 0     1
T-19.68 0     -19.68 0     2
PAD 0 3 P 0    
-2 19.68 RF  0   90   21.65 0  
-1 0   R  
0  0   R  

*END*
