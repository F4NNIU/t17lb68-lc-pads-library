*PADS-LIBRARY-PCB-DECALS-V9*

LC-TO-277A(SMPC) I 0     0     2 2 2 0 3 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-288.46 123.24 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-288.46 173.24 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
OPEN   4 10 26 -1
-283.46 78.74
-283.46 110.24
51.18 110.24
51.18 78.74
OPEN   4 10 26 -1
-283.46 -78.74
-283.46 -110.24
51.18 -110.24
51.18 -78.74
T-159.45 0     -159.45 0     K
T0     -41.34 0     -41.34 1
T0     41.34 0     41.34 2
PAD 1 3 P 0    
-2 192.91 RF  0   0    216.54 0  
-1 0   R  
0  0   R  
PAD 0 3 P 0    
-2 62.99 RF  0   0    70.87 0  
-1 0   R  
0  0   R  

*END*
