*PADS-LIBRARY-PCB-DECALS-V9*

LC-SMD2920       I 0     0     2 2 4 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-208.74 136.03 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-208.74 186.03 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 3.94 18 -1
-157.48 -108.27
-157.48 108.27
157.48 108.27
157.48 -108.27
-157.48 -108.27
OPEN   4 10 26 -1
-78.74 -94.49
-78.74 -39.37
78.74 39.37
78.74 94.49
OPEN   4 10 26 -1
-78.74 123.03
-203.74 123.03
-203.74 -123.03
-78.74 -123.03
OPEN   4 10 26 -1
78.74 -123.03
203.74 -123.03
203.74 123.03
78.74 123.03
T-145.67 0     -145.67 0     1
T145.67 0     145.67 0     2
PAD 0 3 P 0    
-2 90.55 RF  0   90   220.47 0  
-1 0   R  
0  0   R  

*END*
