*PADS-LIBRARY-PCB-DECALS-V9*

LC-SIP9-P-2.54A  I 0     0     2 2 3 0 9 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-496.06 75.99 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-496.06 125.99 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 15.75 26 -1
-472.44 0    
-488.19 0    
CLOSED 5 10 26 -1
-448.43 -62.99
-448.43 62.99
448.43 62.99
448.43 -62.99
-448.43 -62.99
OPEN   2 10 26 -1
-350  -55.12
-350  55.12
T-400  0     -400  0     1
T-300  0     -300  0     2
T-200  0     -200  0     3
T-100  0     -100  0     4
T0     0     0     0     5
T100   0     100   0     6
T200   0     200   0     7
T300   0     300   0     8
T400   0     400   0     9
PAD 1 3 P 35.43
-2 62.99 RF  0   90   70.87 0  
-1 62.99 RF  0   90   70.87 0  
0  62.99 RF  0   90   70.87 0  
PAD 0 3 P 35.43
-2 62.99 OF  90   70.87 0  
-1 62.99 OF  90   70.87 0  
0  62.99 OF  90   70.87 0  

*END*
