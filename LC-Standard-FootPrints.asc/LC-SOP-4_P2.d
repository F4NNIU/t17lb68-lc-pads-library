*PADS-LIBRARY-PCB-DECALS-V9*

LC-SOP-4_P2.54   I 0     0     2 2 4 0 4 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-157.48 106.43 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-157.48 156.43 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 15.75 26 -1
-27.56 51.18
-43.31 51.18
CIRCLE 2 11.81 26 -1
-127.95 86.61
-139.76 86.61
CLOSED 5 3.94 18 -1
-86.61 82.68
86.61 82.68
86.61 -82.68
-86.61 -82.68
-86.61 82.68
CLOSED 5 10 26 -1
-62.99 78.74
62.99 78.74
62.99 -78.74
-62.99 -78.74
-62.99 78.74
T-118.11 50    -118.11 50    1
T-118.11 -50   -118.11 -50   2
T118.11 -50   118.11 -50   3
T118.11 50    118.11 50    4
PAD 0 3 P 0    
-2 31.5 OF  0    78.74 0  
-1 0   R  
0  0   R  

*END*
