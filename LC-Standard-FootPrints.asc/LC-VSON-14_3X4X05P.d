*PADS-LIBRARY-PCB-DECALS-V9*

LC-VSON-14_3X4X05P I 0     0     2 2 7 0 19 11 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-88.61 80.87 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-88.61 130.87 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 9.84 26 -1
-71.85 -82.68
-81.69 -82.68
CIRCLE 2 3.94 18 -1
-47.24 -39.37
-70.87 -39.37
CLOSED 5 3.94 18 -1
-78.74 -59.06
78.74 -59.06
78.74 59.06
-78.74 59.06
-78.74 -59.06
OPEN   3 7.87 26 -1
-82.68 -43.31
-82.68 -62.99
-75.98 -62.99
OPEN   3 7.87 26 -1
-82.68 43.31
-82.68 62.99
-75.98 62.99
OPEN   3 7.87 26 -1
75.98 -62.99
82.68 -62.99
82.68 -43.31
OPEN   3 7.87 26 -1
75.98 62.99
82.68 62.99
82.68 43.31
T0     -9.06 0     -9.06 $$$1
T0     -27.17 0     -27.17 $$$2
T0     9.05  0     9.05  $$$3
T0     27.17 0     27.17 $$$4
T0     -0    0     -0    0
T-59.06 -57.09 -59.06 -57.09 1
T-39.37 -57.09 -39.37 -57.09 2
T-19.68 -57.09 -19.68 -57.09 3
T0     -57.09 0     -57.09 4
T19.69 -57.09 19.69 -57.09 5
T39.37 -57.09 39.37 -57.09 6
T59.06 -57.09 59.06 -57.09 7
T59.06 57.09 59.06 57.09 8
T39.37 57.09 39.37 57.09 9
T19.69 57.09 19.69 57.09 10
T0     57.09 0     57.09 11
T-19.68 57.09 -19.68 57.09 12
T-39.37 57.09 -39.37 57.09 13
T-59.06 57.09 -59.06 57.09 14
PAD 1 4 P 0    
-2 7.87 OF  0    173.23 0  
-1 0   R  
0  0   R  
21 11.87 OF  0    177.23 0  
PAD 2 4 P 0    
-2 7.87 OF  0    173.23 0  
-1 0   R  
0  0   R  
21 11.87 OF  0    177.23 0  
PAD 3 4 P 0    
-2 7.87 OF  0    173.23 0  
-1 0   R  
0  0   R  
21 11.87 OF  0    177.23 0  
PAD 4 4 P 0    
-2 7.87 OF  0    173.23 0  
-1 0   R  
0  0   R  
21 11.87 OF  0    177.23 0  
PAD 5 4 P 0    
-2 62.2 RF  0   0    112.2 0  
-1 0   R  
0  0   R  
21 66.2 RF  0   0    116.2 0  
PAD 0 4 P 0    
-2 9.84 OF  90   27.56 0  
-1 0   R  
0  0   R  
21 13.84 OF  90   31.56 0  
PAD 15 4 P 0    
-2 9.84 OF  90   27.56 0  
-1 0   R  
0  0   R  
21 13.84 OF  90   31.56 0  
PAD 16 4 P 0    
-2 9.84 OF  90   27.56 0  
-1 0   R  
0  0   R  
21 13.84 OF  90   31.56 0  
PAD 17 4 P 0    
-2 9.84 OF  90   27.56 0  
-1 0   R  
0  0   R  
21 13.84 OF  90   31.56 0  
PAD 18 4 P 0    
-2 9.84 OF  90   27.56 0  
-1 0   R  
0  0   R  
21 13.84 OF  90   31.56 0  
PAD 19 4 P 0    
-2 9.84 OF  90   27.56 0  
-1 0   R  
0  0   R  
21 13.84 OF  90   31.56 0  

*END*
