*PADS-LIBRARY-PCB-DECALS-V9*

LC-0805_LED_S3   I 0     0     2 2 11 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-106.36 52.68 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-106.36 102.68 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
OPEN   2 3.94 18 -1
-7.87 27.56
3.94  39.37
OPEN   2 3.94 18 -1
0     23.62
11.81 35.43
OPEN   2 3.94 18 -1
-15.75 0    
15.75 0    
OPEN   2 3.94 18 -1
-7.87 -15.75
-7.87 15.75
CLOSED 4 3.94 18 -1
-7.87 0    
7.87  -15.75
7.87  15.75
-7.87 0    
OPEN   5 7.87 26 -1
-17.72 40.75
-69.68 40.75
-91.14 0    
-69.68 -40.75
-17.72 -40.75
OPEN   4 7.87 26 -1
17.72 -40.75
69.68 -40.75
69.68 40.75
17.72 40.75
OPEN   2 7.87 26 -1
-69.68 -40.75
-69.68 40.75
OPEN   2 8 19 -1
-102.36 -19.68
-102.36 19.68
OPEN   2 8 19 -1
98.43 -19.68
98.43 19.68
OPEN   2 8 19 -1
78.74 0    
118.11 0    
T35.04 0     35.04 0     A
T-35.04 0     -35.04 0     K
PAD 0 3 P 0    
-2 45.67 RF  0   90   57.87 0  
-1 0   R  
0  0   R  

*END*
