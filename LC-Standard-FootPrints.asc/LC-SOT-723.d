*PADS-LIBRARY-PCB-DECALS-V9*

LC-SOT-723       I 0     0     2 2 2 0 3 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-31.5 35.56 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-31.5 85.56 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
OPEN   3 7.87 26 -1
-3.94 23.62
-15.75 23.62
-15.75 19.68
OPEN   3 7.87 26 -1
-15.75 -19.68
-15.75 -23.62
-3.94 -23.62
T19.69 -15.75 19.69 -15.75 1
T19.69 15.75 19.69 15.75 2
T-19.68 -0    -19.68 -0    3
PAD 0 3 P 0    
-2 17.72 RF  0   0    23.62 0  
-1 0   R  
0  0   R  

*END*
