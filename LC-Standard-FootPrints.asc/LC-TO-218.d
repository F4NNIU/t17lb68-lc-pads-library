*PADS-LIBRARY-PCB-DECALS-V9*

LC-TO-218        I 0     0     2 2 3 0 3 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-342.52 142.92 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-342.52 192.92 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 15.75 26 -1
-318.9 0    
-334.65 0    
OPEN   2 10 26 -1
-299.21 106.3
299.21 106.3
CLOSED 5 10 26 -1
299.21 -66.93
299.21 129.92
-299.21 129.92
-299.21 -66.93
299.21 -66.93
T-215.75 0     -215.75 0     1
T0     0     0     0     2
T215.75 0     215.75 0     3
PAD 1 3 P 62.99
-2 98.43 S   0  
-1 98.43 S   0  
0  98.43 S   0  
PAD 0 3 P 62.99
-2 98.43 R  
-1 98.43 R  
0  98.43 R  

*END*
