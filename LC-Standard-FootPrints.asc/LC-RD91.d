*PADS-LIBRARY-PCB-DECALS-V9*

LC-RD91          I 0     0     2 2 4 0 3 3 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-62.98 -71.36 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
-62.98 -121.36 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
CIRCLE 2 10 26 -1
679.13 0    
501.97 0    
CIRCLE 2 10 26 -1
-501.97 0    
-679.13 0    
CIRCLE 2 10 26 -1
433.07 0    
-433.07 0    
CLOSED 9 10 26 -1
-692.87 167.12 1212 1189 -787.97 -199.13 -393.13 195.71
-689.06 -172.8
-280.43 -453.3 2382 642 -531.58 -533.29 531.58 529.87
284.82 -450.55
689.82 -172.36 3002 1191 393.13 -199.13 787.97 195.71
691.34 168.04
280.44 449.88 582 642 -531.58 -533.29 531.58 529.87
-284.82 447.13
-692.87 167.12
T204.72 133.07 204.72 133.07 1
T0     -244.09 0     -244.09 2
T-192.91 149.61 -192.91 149.61 3
PAD 0 3 P 47.24 123   236.22 0    
-2 98.43 OF  123  295.28 0  
-1 98.43 OF  123  295.28 0  
0  98.43 OF  123  295.28 0  
PAD 2 3 P 47.24 0     236.22 0    
-2 98.43 OF  0    295.28 0  
-1 98.43 OF  0    295.28 0  
0  98.43 OF  0    295.28 0  
PAD 3 3 P 47.24 57    236.22 0    
-2 98.43 OF  57   295.28 0  
-1 98.43 OF  57   295.28 0  
0  98.43 OF  57   295.28 0  

*END*
