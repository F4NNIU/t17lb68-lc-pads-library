*PADS-LIBRARY-PCB-DECALS-V9*

LC-LL-34         I 0     0     2 2 7 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-102.43 55.32 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-102.43 105.32 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 3.94 18 -1
-70.87 29.53
70.87 29.53
70.87 -29.53
-70.87 -29.53
-70.87 29.53
OPEN   2 10 26 -1
-70.87 42.32
70.87 42.32
OPEN   2 10 26 -1
-70.87 -42.32
70.87 -42.32
OPEN   2 10 26 -1
-38.39 -42.32
-38.39 42.32
OPEN   2 8 19 -1
-98.43 -19.68
-98.43 19.68
OPEN   2 8 19 -1
96.46 0    
135.83 0    
OPEN   2 8 19 -1
116.14 -19.68
116.14 19.68
T68.9  0     68.9  0     1
T-68.9 0     -68.9 0     2
PAD 0 3 P 0    
-2 35.43 RF  0   90   59.06 0  
-1 0   R  
0  0   R  

*END*
