*PADS-LIBRARY-PCB-DECALS-V9*

LC-LL-35         I 0     0     2 2 7 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-141.8 75.01 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-141.8 125.01 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 3.94 18 -1
-98.43 -51.18
-98.43 51.18
98.43 51.18
98.43 -51.18
-98.43 -51.18
OPEN   2 10 26 -1
-51.18 -62.01
-51.18 62.01
OPEN   2 10 26 -1
-102.36 -62.01
102.36 -62.01
OPEN   2 10 26 -1
-102.36 62.01
102.36 62.01
OPEN   2 8 19 -1
155.51 -15.75
155.51 23.62
OPEN   2 8 19 -1
135.83 3.94 
175.2 3.94 
OPEN   2 8 19 -1
-137.8 -19.68
-137.8 19.68
T94.49 0     94.49 0     1
T-94.49 0     -94.49 0     2
PAD 0 3 P 0    
-2 59.06 RF  0   90   98.43 0  
-1 0   R  
0  0   R  

*END*
