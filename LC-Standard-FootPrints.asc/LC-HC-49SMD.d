*PADS-LIBRARY-PCB-DECALS-V9*

LC-HC-49SMD      I 0     0     2 2 2 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-295.27 107.49 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-295.27 157.49 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
OPEN   4 10 26 -1
-224.41 -64.96
-224.41 -94.49
224.41 -94.49
224.41 -64.96
OPEN   4 10 26 -1
224.41 64.96
224.41 94.49
-224.41 94.49
-224.41 64.96
T-187.01 0     -187.01 0     1
T187.01 0     187.01 0     2
PAD 0 3 P 0    
-2 78.74 RF  0   0    216.54 0  
-1 0   R  
0  0   R  

*END*
