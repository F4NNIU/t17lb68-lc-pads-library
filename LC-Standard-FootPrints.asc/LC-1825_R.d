*PADS-LIBRARY-PCB-DECALS-V9*

LC-1825_R        I 0     0     2 2 3 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-129.92 157.8 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-129.92 207.8 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 3.94 18 -1
-90.55 -125.98
-90.55 125.98
90.55 125.98
90.55 -125.98
-90.55 -125.98
OPEN   4 7.87 26 -1
-47.24 145.87
-125.98 145.87
-125.98 -145.87
-47.24 -145.87
OPEN   4 7.87 26 -1
47.24 -145.87
125.98 -145.87
125.98 145.87
47.24 145.87
T-84.65 0     -84.65 0     1
T84.65 0     84.65 0     2
PAD 0 3 P 0    
-2 59.06 RF  0   90   268.11 0  
-1 0   R  
0  0   R  

*END*
