*PADS-LIBRARY-PCB-DECALS-V9*

LC-TSSOP-16      I 0     0     2 2 5 0 16 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-127.95 153.67 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-127.95 203.67 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 15.75 26 -1
-66.93 -39.37
-82.68 -39.37
CIRCLE 2 11.81 26 -1
-110.24 -129.92
-122.05 -129.92
CIRCLE 2 3.94 18 -1
-45.28 -53.15
-84.65 -53.15
CLOSED 5 10 26 -1
-100.39 -62.99
100.39 -62.99
100.39 62.99
-100.39 62.99
-100.39 -62.99
CLOSED 5 3.94 18 -1
-100.39 -88.58
100.39 -88.58
100.39 88.58
-100.39 88.58
-100.39 -88.58
T-89.57 -112.2 -89.57 -112.2 1
T-63.98 -112.2 -63.98 -112.2 2
T-38.39 -112.2 -38.39 -112.2 3
T-12.8 -112.2 -12.8 -112.2 4
T12.8  -112.2 12.8  -112.2 5
T38.39 -112.2 38.39 -112.2 6
T63.98 -112.2 63.98 -112.2 7
T89.57 -112.2 89.57 -112.2 8
T89.57 112.2 89.57 112.2 9
T63.98 112.2 63.98 112.2 10
T38.39 112.2 38.39 112.2 11
T12.8  112.2 12.8  112.2 12
T-12.8 112.2 -12.8 112.2 13
T-38.39 112.2 -38.39 112.2 14
T-63.98 112.2 -63.98 112.2 15
T-89.57 112.2 -89.57 112.2 16
PAD 0 3 P 0    
-2 12.6 OF  90   66.93 0  
-1 0   R  
0  0   R  

*END*
