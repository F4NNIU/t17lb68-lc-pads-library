*PADS-LIBRARY-PCB-DECALS-V9*

LC-QFN-32_5X5X05P I 0     0     2 2 16 0 33 18 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-120.11 128.11 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-120.11 178.11 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 3.94 18 -1
-51.18 -70.87
-90.55 -70.87
CIRCLE 2 11.81 26 -1
-92.52 -125.98
-104.33 -125.98
CLOSED 5 3.94 18 -1
100.39 -100.39
100.39 100.39
-100.39 100.39
-100.39 -100.39
100.39 -100.39
OPEN   3 7.87 26 -1
-90.35 -106.3
-106.3 -106.3
-106.3 -90.35
OPEN   3 7.87 26 -1
90.35 -106.3
106.3 -106.3
106.3 -90.35
OPEN   3 7.87 26 -1
90.35 106.3
106.3 106.3
106.3 90.35
OPEN   3 7.87 26 -1
-90.35 106.3
-106.3 106.3
-106.3 90.35
COPCLS 5 0.1 23 -1
21.65 -41.34
41.34 -41.34
41.34 -21.65
21.65 -21.65
21.65 -41.34
COPCLS 5 0.1 23 -1
-9.84 -41.34
9.84  -41.34
9.84  -21.65
-9.84 -21.65
-9.84 -41.34
COPCLS 5 0.1 23 -1
-41.34 -41.34
-21.65 -41.34
-21.65 -21.65
-41.34 -21.65
-41.34 -41.34
COPCLS 5 0.1 23 -1
-9.84 -9.84
9.84  -9.84
9.84  9.84 
-9.84 9.84 
-9.84 -9.84
COPCLS 5 0.1 23 -1
-41.34 -9.84
-21.65 -9.84
-21.65 9.84 
-41.34 9.84 
-41.34 -9.84
COPCLS 5 0.1 23 -1
21.65 -9.84
41.34 -9.84
41.34 9.84 
21.65 9.84 
21.65 -9.84
COPCLS 5 0.1 23 -1
-9.84 21.65
9.84  21.65
9.84  41.34
-9.84 41.34
-9.84 21.65
COPCLS 5 0.1 23 -1
21.65 21.65
41.34 21.65
41.34 41.34
21.65 41.34
21.65 21.65
COPCLS 5 0.1 23 -1
-41.34 21.65
-21.65 21.65
-21.65 41.34
-41.34 41.34
-41.34 21.65
T0     0     0     0     0
T-68.9 -100.39 -68.9 -100.39 1
T-49.21 -100.39 -49.21 -100.39 2
T-29.53 -100.39 -29.53 -100.39 3
T-9.84 -100.39 -9.84 -100.39 4
T9.84  -100.39 9.84  -100.39 5
T29.53 -100.39 29.53 -100.39 6
T49.21 -100.39 49.21 -100.39 7
T68.9  -100.39 68.9  -100.39 8
T100.39 -68.9 100.39 -68.9 9
T100.39 -49.21 100.39 -49.21 10
T100.39 -29.53 100.39 -29.53 11
T100.39 -9.84 100.39 -9.84 12
T100.39 9.84  100.39 9.84  13
T100.39 29.53 100.39 29.53 14
T100.39 49.21 100.39 49.21 15
T100.39 68.9  100.39 68.9  16
T68.9  100.39 68.9  100.39 17
T49.21 100.39 49.21 100.39 18
T29.53 100.39 29.53 100.39 19
T9.84  100.39 9.84  100.39 20
T-9.84 100.39 -9.84 100.39 21
T-29.53 100.39 -29.53 100.39 22
T-49.21 100.39 -49.21 100.39 23
T-68.9 100.39 -68.9 100.39 24
T-100.39 68.9  -100.39 68.9  25
T-100.39 49.21 -100.39 49.21 26
T-100.39 29.53 -100.39 29.53 27
T-100.39 9.84  -100.39 9.84  28
T-100.39 -9.84 -100.39 -9.84 29
T-100.39 -29.53 -100.39 -29.53 30
T-100.39 -49.21 -100.39 -49.21 31
T-100.39 -68.9 -100.39 -68.9 32
PAD 1 5 P 0    
-2 137.8 RF  0   90   137.8 0  
-1 0   R  
0  0   R  
21 141.8 RF  0   90   141.8 0  
23 0   RF  0   90   0  0  
PAD 0 4 P 0    
-2 11.02 OF  90   35.43 0  
-1 0   R  
0  0   R  
21 15.02 OF  90   39.43 0  
PAD 10 4 P 0    
-2 11.02 OF  0    35.43 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    39.43 0  
PAD 11 4 P 0    
-2 11.02 OF  0    35.43 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    39.43 0  
PAD 12 4 P 0    
-2 11.02 OF  0    35.43 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    39.43 0  
PAD 13 4 P 0    
-2 11.02 OF  0    35.43 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    39.43 0  
PAD 14 4 P 0    
-2 11.02 OF  0    35.43 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    39.43 0  
PAD 15 4 P 0    
-2 11.02 OF  0    35.43 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    39.43 0  
PAD 16 4 P 0    
-2 11.02 OF  0    35.43 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    39.43 0  
PAD 17 4 P 0    
-2 11.02 OF  0    35.43 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    39.43 0  
PAD 26 4 P 0    
-2 11.02 OF  0    35.43 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    39.43 0  
PAD 27 4 P 0    
-2 11.02 OF  0    35.43 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    39.43 0  
PAD 28 4 P 0    
-2 11.02 OF  0    35.43 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    39.43 0  
PAD 29 4 P 0    
-2 11.02 OF  0    35.43 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    39.43 0  
PAD 30 4 P 0    
-2 11.02 OF  0    35.43 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    39.43 0  
PAD 31 4 P 0    
-2 11.02 OF  0    35.43 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    39.43 0  
PAD 32 4 P 0    
-2 11.02 OF  0    35.43 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    39.43 0  
PAD 33 4 P 0    
-2 11.02 OF  0    35.43 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    39.43 0  

*END*
