*PADS-LIBRARY-PCB-DECALS-V9*

LC-CASE-A_3216   I 0     0     2 2 6 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-122.11 52.45 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-122.11 102.45 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
OPEN   2 10 26 -1
99.88 -39.45
99.88 39.45
OPEN   4 10 26 -1
-23.62 -39.45
-99.88 -39.45
-99.88 39.45
-23.62 39.45
OPEN   6 10 26 -1
23.62 -39.45
99.88 -39.45
117.05 -23.62
117.05 23.62
99.88 39.45
23.62 39.45
OPEN   2 8 19 -1
-118.11 -19.68
-118.11 19.68
OPEN   2 8 19 -1
137.8 0    
177.17 0    
OPEN   2 8 19 -1
157.48 -19.68
157.48 19.68
T51.57 0     51.57 0     1
T-51.57 0     -51.57 0     2
PAD 0 3 P 0    
-2 53.15 RF  0   0    70.87 0  
-1 0   R  
0  0   R  

*END*
