*PADS-LIBRARY-PCB-DECALS-V9*

LC-1206_L        I 0     0     2 2 2 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-96.65 59.18 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-96.65 109.18 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 1.97 18 -1
-62.99 -31.5
-62.99 31.5 
62.99 31.5 
62.99 -31.5
-62.99 -31.5
CLOSED 5 7.87 26 -1
-92.72 -47.24
-92.72 47.24
92.72 47.24
92.72 -47.24
-92.72 -47.24
T-58.66 0     -58.66 0     1
T58.66 0     58.66 0     2
PAD 0 3 P 0    
-2 44.49 RF  0   90   70.87 0  
-1 0   R  
0  0   R  

*END*
