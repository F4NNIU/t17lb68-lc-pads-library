*PADS-LIBRARY-PCB-DECALS-V9*

LC-SMD-5032_2P   I 0     0     2 2 3 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-135.91 75.99 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-135.91 125.99 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 3.94 18 -1
-98.43 62.99
98.43 62.99
98.43 -62.99
-98.43 -62.99
-98.43 62.99
OPEN   4 10 26 -1
-130.91 -27.56
-130.91 -62.99
130.91 -62.99
130.91 -27.56
OPEN   4 10 26 -1
130.91 27.56
130.91 62.99
-130.91 62.99
-130.91 27.56
T-80.71 0     -80.71 0     1
T80.71 0     80.71 0     2
PAD 0 3 P 0    
-2 74.8 RF  0   90   94.49 0  
-1 0   R  
0  0   R  

*END*
