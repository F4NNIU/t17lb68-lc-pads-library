*PADS-LIBRARY-PCB-DECALS-V9*

LC-SSOP-10_150MIL I 0     0     2 2 5 0 10 6 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-116.14 145.8 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-116.14 195.8 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 15.75 26 -1
-62.99 -23.62
-78.74 -23.62
CIRCLE 2 9.84 26 -1
-101.38 -114.17
-111.22 -114.17
CIRCLE 2 3.94 18 -1
-43.31 -43.31
-82.68 -43.31
CLOSED 5 7.87 18 -1
-98.43 -78.74
98.43 -78.74
98.43 78.74
-98.43 78.74
-98.43 -78.74
CLOSED 5 10 26 -1
-98.43 -51.18
98.43 -51.18
98.43 55.12
-98.43 55.12
-98.43 -51.18
T-78.74 -102.36 -78.74 -102.36 1
T-39.37 -102.36 -39.37 -102.36 2
T0     -102.36 0     -102.36 3
T39.37 -102.36 39.37 -102.36 4
T78.74 -102.36 78.74 -102.36 5
T78.74 102.36 78.74 102.36 6
T39.37 102.36 39.37 102.36 7
T0     102.36 0     102.36 8
T-39.37 102.36 -39.37 102.36 9
T-78.74 102.36 -78.74 102.36 10
PAD 0 3 P 0    
-2 23.62 OF  90   70.87 0  
-1 0   R  
0  0   R  
PAD 6 3 P 0    
-2 23.62 OF  90   70.87 0  
-1 0   R  
0  0   R  
PAD 7 3 P 0    
-2 23.62 OF  90   70.87 0  
-1 0   R  
0  0   R  
PAD 8 3 P 0    
-2 23.62 OF  90   70.87 0  
-1 0   R  
0  0   R  
PAD 9 3 P 0    
-2 23.62 OF  90   70.87 0  
-1 0   R  
0  0   R  
PAD 10 3 P 0    
-2 23.62 OF  90   70.87 0  
-1 0   R  
0  0   R  

*END*
