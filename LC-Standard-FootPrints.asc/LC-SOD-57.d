*PADS-LIBRARY-PCB-DECALS-V9*

LC-SOD-57        I 0     0     2 2 7 0 2 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-232.35 83    0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-232.35 133   0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 10 26 -1
-80   70   
80    70   
80    -70  
-80   -70  
-80   70   
OPEN   2 10 26 -1
-114.17 0    
-80   0    
OPEN   2 10 26 -1
80    0    
114.17 0    
OPEN   4 10 26 -1
-55.12 70   
-55.12 -70  
-47.24 -70  
-47.24 70   
OPEN   2 8 19 -1
248.03 -19.68
248.03 19.68
OPEN   2 8 19 -1
228.35 0    
267.72 0    
OPEN   2 8 19 -1
-228.35 -19.68
-228.35 19.68
T175   0     175   0     1
T-175  0     -175  0     2
PAD 1 3 P 47.24
-2 78.74 RF  0   0    78.74 0  
-1 78.74 RF  0   0    78.74 0  
0  78.74 RF  0   0    78.74 0  
PAD 2 3 P 47.24
-2 78.74 R  
-1 78.74 R  
0  78.74 R  

*END*
