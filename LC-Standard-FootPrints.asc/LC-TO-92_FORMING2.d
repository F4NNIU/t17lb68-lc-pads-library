*PADS-LIBRARY-PCB-DECALS-V9*

LC-TO-92_FORMING2 I 0     0     2 2 2 0 3 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-123.62 140.81 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-123.62 190.81 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
OPEN   2 10 26 -1
106.12 77.8  400 1000 -141.13 -152.19 138.87 127.81
-108.37 77.8 
OPEN   2 10 26 -1
-102.36 -47.24
98.43 -47.24
T-100  15.25 -100  15.25 1
T0     63.75 0     63.75 2
T100   15.25 100   15.25 3
PAD 0 3 P 31.5 
-2 47.24 OF  90   78.74 0  
-1 47.24 OF  90   78.74 0  
0  47.24 OF  90   78.74 0  

*END*
