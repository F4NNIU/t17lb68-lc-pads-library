*PADS-LIBRARY-PCB-DECALS-V9*

LC-TO-3P         I 0     0     2 2 3 0 3 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-312.09 99.61 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-312.09 149.61 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 15.75 26 -1
-204.72 -82.68
-220.47 -82.68
CLOSED 5 10 26 -1
307.09 -110.24
307.09 86.61
-307.09 86.61
-307.09 -110.24
307.09 -110.24
OPEN   2 10 26 -1
-307.09 66.93
307.09 66.93
T-214.57 0     -214.57 0     1
T0     0     0     0     2
T214.57 0     214.57 0     3
PAD 1 3 P 62.99
-2 98.43 RF  0   0    118.11 0  
-1 98.43 RF  0   0    118.11 0  
0  98.43 RF  0   0    118.11 0  
PAD 0 3 P 62.99
-2 98.43 OF  0    118.11 0  
-1 98.43 OF  0    118.11 0  
0  98.43 OF  0    118.11 0  

*END*
