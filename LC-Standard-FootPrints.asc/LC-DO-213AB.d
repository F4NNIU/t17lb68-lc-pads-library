*PADS-LIBRARY-PCB-DECALS-V9*

LC-DO-213AB      I 0     0     2 2 7 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-137.86 80.91 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-137.86 130.91 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 3.94 18 -1
-98.43 51.18
98.43 51.18
98.43 -51.18
-98.43 -51.18
-98.43 51.18
OPEN   2 10 26 -1
-98.43 67.91
98.43 67.91
OPEN   2 10 26 -1
-98.43 -67.91
98.43 -67.91
OPEN   2 10 26 -1
-55.12 -67.91
-55.12 66.93
OPEN   2 8 19 -1
131.9 0    
171.27 0    
OPEN   2 8 19 -1
151.59 -19.68
151.59 19.69
OPEN   2 8 19 -1
-133.86 -19.68
-133.86 19.68
T94.5  0     94.5  0     1
T-94.5 0     -94.5 0     2
PAD 0 3 P 0    
-2 51.18 RF  0   90   110.24 0  
-1 0   R  
0  0   R  

*END*
