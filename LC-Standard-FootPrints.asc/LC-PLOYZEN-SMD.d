*PADS-LIBRARY-PCB-DECALS-V9*

LC-PLOYZEN-SMD   I 0     0     2 2 3 0 3 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-91.61 99.61 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-91.61 149.61 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 3.94 18 -1
-78.74 78.74
78.74 78.74
78.74 -78.74
-78.74 -78.74
-78.74 78.74
OPEN   4 10 26 -1
-86.61 59.06
-86.61 86.61
86.61 86.61
86.61 59.06
OPEN   4 10 26 -1
-86.61 -59.06
-86.61 -86.61
86.61 -86.61
86.61 -59.06
T-70.87 0     -70.87 0     1
T70.87 25    70.87 25    2
T70.87 -25   70.87 -25   3
PAD 1 3 P 0    
-2 39.37 RF  0   90   87.01 0  
-1 0   R  
0  0   R  
PAD 0 3 P 0    
-2 37.01 RF  0   0    39.37 0  
-1 0   R  
0  0   R  

*END*
