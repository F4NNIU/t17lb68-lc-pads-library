*PADS-LIBRARY-PCB-DECALS-V9*

LC-SOT-753       I 0     0     2 2 2 0 5 3 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-73.82 69.02 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-73.82 119.02 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 10 26 -1
-11.81 -51.18
11.81 -51.18
11.81 51.18
-11.81 51.18
-11.81 -51.18
CLOSED 5 3.94 18 -1
31.5  -59.05
31.5  59.06
-31.5 59.06
-31.5 -59.05
31.5  -59.05
T49.21 -37.4 49.21 -37.4 1
T49.21 0     49.21 0     2
T49.21 37.4  49.21 37.4  3
T-49.21 37.4  -49.21 37.4  4
T-49.21 -37.4 -49.21 -37.4 5
PAD 0 3 P 0    
-2 21.65 RF  0   0    49.21 0  
-1 0   R  
0  0   R  
PAD 4 3 P 0    
-2 21.65 RF  0   0    49.21 0  
-1 0   R  
0  0   R  
PAD 5 3 P 0    
-2 21.65 RF  0   0    49.21 0  
-1 0   R  
0  0   R  

*END*
