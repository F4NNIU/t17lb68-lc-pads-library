*PADS-LIBRARY-PCB-DECALS-V9*

LC-GDTS_SMD      I 0     0     2 2 3 1 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-149.61 181.11 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
-149.61 131.11 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
OPEN   4 10 26 -1
-118.11 70.87
-118.11 118.11
118.11 118.11
118.11 70.87
OPEN   4 10 26 -1
-118.11 -70.87
-118.11 -118.11
118.11 -118.11
118.11 -70.87
CLOSED 5 3.94 18 -1
118.11 -110.24
118.11 110.24
-118.11 110.24
-118.11 -110.24
118.11 -110.24
18.6   -60    90 26  70   10 0 0 0 0 "Regular Tahoma"
GDTs
T-110.24 0     -110.24 0     1
T110.24 0     110.24 0     2
PAD 0 3 P 0    
-2 78.74 RF  0   90   118.11 0  
-1 0   R  
0  0   R  

*END*
