*PADS-LIBRARY-PCB-DECALS-V9*

LC-TSOP-6        I 0     0     2 2 3 0 6 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-76.77 70.99 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-76.77 120.99 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 9.84 26 -1
67.91 -70.87
58.07 -70.87
CLOSED 5 3.94 18 -1
35.43 -61.02
35.43 61.02
-35.43 61.02
-35.43 -61.02
35.43 -61.02
CLOSED 5 10 26 -1
-19.69 -51.18
-19.69 51.18
19.68 51.18
19.68 -51.18
-19.69 -51.18
T55.12 -37.4 55.12 -37.4 1
T55.12 0     55.12 0     2
T55.12 37.4  55.12 37.4  3
T-55.12 37.4  -55.12 37.4  4
T-55.12 0     -55.12 0     5
T-55.12 -37.4 -55.12 -37.4 6
PAD 0 3 P 0    
-2 23.62 RF  0   0    43.31 0  
-1 0   R  
0  0   R  

*END*
