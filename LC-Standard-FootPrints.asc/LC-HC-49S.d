*PADS-LIBRARY-PCB-DECALS-V9*

LC-HC-49S        I 0     0     2 2 1 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-217.5 101.5 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-217.5 151.5 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 10 26 -1
-124  88.5  900 1800 -212.5 -88.5 -35.5 88.5 
-124  -88.5
124   -88.5 2700 1800 35.5  -88.5 212.5 88.5 
124   88.5 
-124  88.5 
T-96.46 0     -96.46 0     1
T96.46 0     96.46 0     2
PAD 0 3 P 27.56
-2 62.99 R  
-1 62.99 R  
0  62.99 R  

*END*
