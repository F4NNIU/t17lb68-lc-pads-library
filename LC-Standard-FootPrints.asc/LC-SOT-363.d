*PADS-LIBRARY-PCB-DECALS-V9*

LC-SOT-363       I 0     0     2 2 4 0 6 3 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-60.07 60.24 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-60.07 110.24 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 9.84 26 -1
52.17 -51.57
42.32 -51.57
CLOSED 5 3.94 18 -1
-27.56 -43.31
27.56 -43.31
27.56 43.31
-27.56 43.31
-27.56 -43.31
OPEN   2 10 26 -1
-27.56 -47.24
27.56 -47.24
OPEN   2 10 26 -1
-27.56 47.24
27.56 47.24
T38.39 -25.59 38.39 -25.59 1
T38.39 0     38.39 0     2
T38.39 25.59 38.39 25.59 3
T-38.39 25.59 -38.39 25.59 4
T-38.39 0     -38.39 0     5
T-38.39 -25.59 -38.39 -25.59 6
PAD 0 4 P 0    
-2 16.54 RF  0   0    39.37 0  
-1 0   R  
0  0   R  
21 20.54 RF  0   0    43.37 0  
PAD 4 4 P 0    
-2 16.54 RF  0   0    39.37 0  
-1 0   R  
0  0   R  
21 20.54 RF  0   0    43.37 0  
PAD 6 4 P 0    
-2 16.54 RF  0   0    39.37 0  
-1 0   R  
0  0   R  
21 20.54 RF  0   0    43.37 0  

*END*
