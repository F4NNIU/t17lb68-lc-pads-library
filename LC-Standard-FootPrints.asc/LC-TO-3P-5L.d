*PADS-LIBRARY-PCB-DECALS-V9*

LC-TO-3P-5L      I 0     0     2 2 3 0 5 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-125.98 137.02 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-125.98 187.02 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 15.75 26 -1
-102.36 0    
-118.11 0    
OPEN   2 10 26 -1
-82.09 102.36
532.09 102.36
CLOSED 5 10 26 -1
-82.09 -64.96
532.09 -64.96
532.09 124.02
-82.09 124.02
-82.09 -64.96
T0     0     0     0     1
T150   0     150   0     2
T250   0     250   0     3
T350   0     350   0     4
T450   0     450   0     5
PAD 1 3 P 55.12
-2 86.61 S   0  
-1 86.61 S   0  
0  86.61 S   0  
PAD 0 3 P 55.12
-2 86.61 R  
-1 86.61 R  
0  86.61 R  

*END*
