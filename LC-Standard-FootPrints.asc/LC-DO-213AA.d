*PADS-LIBRARY-PCB-DECALS-V9*

LC-DO-213AA      I 0     0     2 2 7 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-110.3 62.21 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-110.3 112.21 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 3.94 18 -1
-70.87 -31.5
-70.87 31.5 
70.87 31.5 
70.87 -31.5
-70.87 -31.5
OPEN   2 10 26 -1
-29.53 -49.21
-29.53 49.21
OPEN   2 10 26 -1
-74.8 -49.21
74.8  -49.21
OPEN   2 10 26 -1
-74.8 49.21
74.8  49.21
OPEN   2 8 19 -1
124.02 -19.68
124.02 19.68
OPEN   2 8 19 -1
104.33 0    
143.7 0    
OPEN   2 8 19 -1
-106.3 -19.68
-106.3 19.68
T66.93 0     66.93 0     1
T-66.93 0     -66.93 0     2
PAD 0 3 P 0    
-2 49.21 RF  0   90   72.83 0  
-1 0   R  
0  0   R  

*END*
