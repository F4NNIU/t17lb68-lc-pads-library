*PADS-LIBRARY-PCB-DECALS-V9*

LC-TO-220(TO-220-3) I 0     0     2 2 3 0 3 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-213.66 123.24 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-213.66 173.24 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 15.75 26 -1
-161.42 0    
-177.17 0    
OPEN   2 10 26 -1
-208.66 90.55
208.66 90.55
CLOSED 5 10 26 -1
-208.66 110.24
208.66 110.24
208.66 -78.74
-208.66 -78.74
-208.66 110.24
T-100  0     -100  0     1
T0     0     0     0     2
T100   0     100   0     3
PAD 1 3 P 51.18
-2 78.74 RF  0   90   98.43 0  
-1 78.74 RF  0   90   98.43 0  
0  78.74 RF  0   90   98.43 0  
PAD 0 3 P 51.18
-2 78.74 OF  90   98.43 0  
-1 78.74 OF  90   98.43 0  
0  78.74 OF  90   98.43 0  

*END*
