*PADS-LIBRARY-PCB-DECALS-V9*

LC-1812_R        I 0     0     2 2 2 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-124.02 91.07 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-124.02 141.07 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
OPEN   4 7.87 26 -1
-59.06 -79.13
-120.08 -79.13
-120.08 79.13
-59.06 79.13
OPEN   4 7.87 26 -1
59.05 -79.13
120.08 -79.13
120.08 79.13
59.05 79.13
T-84.65 0     -84.65 0     1
T84.65 0     84.65 0     2
PAD 0 3 P 0    
-2 47.24 RF  0   90   134.65 0  
-1 0   R  
0  0   R  

*END*
