*PADS-LIBRARY-PCB-DECALS-V9*

LC-1808_R        I 0     0     2 2 3 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-118.11 69.02 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-118.11 119.02 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 3.94 18 -1
-88.58 -39.37
-88.58 39.37
88.58 39.37
88.58 -39.37
-88.58 -39.37
OPEN   4 7.87 26 -1
-43.31 -57.09
-114.17 -57.09
-114.17 57.09
-43.31 57.09
OPEN   4 7.87 26 -1
43.31 -57.09
114.17 -57.09
114.17 57.09
43.31 57.09
T-74.8 0     -74.8 0     1
T74.8  0     74.8  0     2
PAD 0 3 P 0    
-2 55.12 RF  0   90   90.55 0  
-1 0   R  
0  0   R  

*END*
