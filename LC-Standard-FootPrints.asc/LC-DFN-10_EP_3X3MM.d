*PADS-LIBRARY-PCB-DECALS-V9*

LC-DFN-10_EP_3X3MM I 0     0     2 2 4 0 11 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-70.87 83.82 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-70.87 133.82 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 11.81 26 -1
-54.06 -86.61
-64.06 -86.61
CLOSED 5 3.94 18 -1
-59.06 -59.06
-59.06 59.06
59.06 59.06
59.06 -59.06
-59.06 -59.06
OPEN   4 7.87 26 -1
59.06 62.99
66.93 62.99
66.93 -62.99
59.06 -62.99
OPEN   4 7.87 26 -1
-59.06 -62.99
-66.93 -62.99
-66.93 62.99
-59.06 62.99
T0     0     0     0     0
T-39.37 -61.02 -39.37 -61.02 1
T-19.69 -61.02 -19.69 -61.02 2
T0     -61.02 0     -61.02 3
T19.68 -61.02 19.68 -61.02 4
T39.37 -61.02 39.37 -61.02 5
T39.37 61.02 39.37 61.02 6
T19.68 61.02 19.68 61.02 7
T0     61.02 0     61.02 8
T-19.69 61.02 -19.69 61.02 9
T-39.37 61.02 -39.37 61.02 10
PAD 1 4 P 0    
-2 70.87 RF  0   0    102.36 0  
-1 0   R  
0  0   R  
21 74.87 RF  0   0    106.36 0  
PAD 0 4 P 0    
-2 11.02 RF  0   90   25.59 0  
-1 0   R  
0  0   R  
21 15.02 RF  0   90   29.59 0  

*END*
