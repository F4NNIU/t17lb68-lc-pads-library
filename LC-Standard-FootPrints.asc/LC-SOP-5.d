*PADS-LIBRARY-PCB-DECALS-V9*

LC-SOP-5         I 0     0     2 2 4 0 5 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-163.39 108.39 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-163.39 158.39 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 15.75 26 -1
-35.43 45.28
-51.18 45.28
CIRCLE 2 15.75 26 -1
-127.95 86.61
-139.76 86.61
CLOSED 5 10 18 -1
-86.61 70.87
86.61 70.87
86.61 -70.87
-86.61 -70.87
-86.61 70.87
CLOSED 5 10 26 -1
-66.93 -70.87
-66.93 70.87
66.93 70.87
66.93 -70.87
-66.93 -70.87
T-124.02 50    -124.02 50    1
T-124.02 -50   -124.02 -50   2
T124.02 -50   124.02 -50   3
T124.02 0     124.02 0     4
T124.02 50    124.02 50    5
PAD 0 3 P 0    
-2 25.59 OF  0    78.74 0  
-1 0   R  
0  0   R  

*END*
