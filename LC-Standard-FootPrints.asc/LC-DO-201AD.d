*PADS-LIBRARY-PCB-DECALS-V9*

LC-DO-201AD      I 0     0     2 2 7 0 2 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-380.77 123.24 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-380.77 173.24 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
OPEN   6 10 26 -1
-114.17 110.24
-114.17 -110.24
-106.3 -110.24
-106.3 110.24
-98.43 110.24
-98.43 -110.24
OPEN   2 10 26 -1
177.17 0    
220.47 0    
OPEN   2 10 26 -1
-220.47 0    
-157.48 0    
CLOSED 5 10 26 -1
-157.48 110.24
177.17 110.24
177.17 -110.24
-157.48 -110.24
-157.48 110.24
OPEN   2 8 19 -1
370.08 0    
409.45 0    
OPEN   2 8 19 -1
389.76 -19.68
389.76 19.69
OPEN   2 8 19 -1
-376.77 -19.68
-376.77 19.69
T300   0     300   0     1
T-300  0     -300  0     2
PAD 1 3 P 66.93
-2 118.11 S   0  
-1 118.11 S   0  
0  118.11 S   0  
PAD 2 3 P 66.93
-2 118.11 R  
-1 118.11 R  
0  118.11 R  

*END*
