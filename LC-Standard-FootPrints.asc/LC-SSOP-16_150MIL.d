*PADS-LIBRARY-PCB-DECALS-V9*

LC-SSOP-16_150MIL I 0     0     2 2 3 0 16 9 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-126.97 145.83 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-126.97 195.83 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 7 10 26 -1
-91.73 -19.68 2700 1800 -111.42 -19.68 -72.05 19.68
-91.73 19.68
-91.73 59.06
90.55 59.06
90.55 -59.06
-91.73 -59.06
-91.73 -19.68
CIRCLE 2 11.81 26 -1
-109.25 -115.16
-121.06 -115.16
CLOSED 5 3.94 18 -1
-98.43 78.74
98.43 78.74
98.43 -78.74
-98.43 -78.74
-98.43 78.74
T-87.5 -103.74 -87.5 -103.74 1
T-62.5 -103.74 -62.5 -103.74 2
T-37.5 -103.74 -37.5 -103.74 3
T-12.5 -103.74 -12.5 -103.74 4
T12.5  -103.74 12.5  -103.74 5
T37.5  -103.74 37.5  -103.74 6
T62.5  -103.74 62.5  -103.74 7
T87.5  -103.74 87.5  -103.74 8
T87.5  103.74 87.5  103.74 9
T62.5  103.74 62.5  103.74 10
T37.5  103.74 37.5  103.74 11
T12.5  103.74 12.5  103.74 12
T-12.5 103.74 -12.5 103.74 13
T-37.5 103.74 -37.5 103.74 14
T-62.5 103.74 -62.5 103.74 15
T-87.5 103.74 -87.5 103.74 16
PAD 0 4 P 0    
-2 15.75 OF  90   64.17 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   68.17 0  
PAD 9 4 P 0    
-2 15.75 OF  90   64.17 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   68.17 0  
PAD 10 4 P 0    
-2 15.75 OF  90   64.17 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   68.17 0  
PAD 11 4 P 0    
-2 15.75 OF  90   64.17 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   68.17 0  
PAD 12 4 P 0    
-2 15.75 OF  90   64.17 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   68.17 0  
PAD 13 4 P 0    
-2 15.75 OF  90   64.17 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   68.17 0  
PAD 14 4 P 0    
-2 15.75 OF  90   64.17 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   68.17 0  
PAD 15 4 P 0    
-2 15.75 OF  90   64.17 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   68.17 0  
PAD 16 4 P 0    
-2 15.75 OF  90   64.17 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   68.17 0  

*END*
