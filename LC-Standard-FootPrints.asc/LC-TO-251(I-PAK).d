*PADS-LIBRARY-PCB-DECALS-V9*

LC-TO-251(I-PAK) I 0     0     2 2 2 0 3 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-142.8 83.87 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-142.8 133.87 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
OPEN   2 10 26 -1
-137.8 55.12
137.8 55.12
CLOSED 7 10 26 -1
-137.8 -55.12
137.8 -55.12
137.8 55.12
137.8 70.87
-137.8 70.87
-137.8 55.12
-137.8 -55.12
T-90.55 0     -90.55 0     1
T0     0     0     0     2
T90.55 0     90.55 0     3
PAD 1 3 P 43.31
-2 62.99 RF  0   90   78.74 0  
-1 62.99 RF  0   90   78.74 0  
0  62.99 RF  0   90   78.74 0  
PAD 0 3 P 43.31
-2 62.99 OF  90   78.74 0  
-1 62.99 OF  90   78.74 0  
0  62.99 OF  90   78.74 0  

*END*
