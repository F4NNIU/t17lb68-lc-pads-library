*PADS-LIBRARY-PCB-DECALS-V9*

LC-SIP8-P-2.54A  I 0     0     2 2 3 0 8 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-446.06 68.12 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-446.06 118.12 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 15.75 26 -1
-422.44 0    
-438.19 0    
CLOSED 5 10 26 -1
-400  -55.12
-400  55.12
400   55.12
400   -55.12
-400  -55.12
OPEN   2 10 26 -1
-300  -55.12
-300  55.12
T-350  0     -350  0     1
T-250  0     -250  0     2
T-150  0     -150  0     3
T-50   0     -50   0     4
T50    0     50    0     5
T150   0     150   0     6
T250   0     250   0     7
T350   0     350   0     8
PAD 1 3 P 35.43
-2 62.99 RF  0   90   70.87 0  
-1 62.99 RF  0   90   70.87 0  
0  62.99 RF  0   90   70.87 0  
PAD 0 3 P 35.43
-2 62.99 OF  90   70.87 0  
-1 62.99 OF  90   70.87 0  
0  62.99 OF  90   70.87 0  

*END*
