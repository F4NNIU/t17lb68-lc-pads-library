*PADS-LIBRARY-PCB-DECALS-V9*

LC-R-6           I 0     0     2 2 7 0 2 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-326.83 154.73 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-326.83 204.73 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
OPEN   4 10 26 -1
-82.68 141.73
-82.68 -141.73
-90.55 -141.73
-90.55 141.73
OPEN   2 10 26 -1
118.11 0    
173.23 0    
OPEN   2 10 26 -1
-173.23 0    
-118.11 0    
CLOSED 5 10 26 -1
-118.11 -141.73
118.11 -141.73
118.11 141.73
-118.11 141.73
-118.11 -141.73
OPEN   2 8 19 -1
-322.83 -19.68
-322.83 19.68
OPEN   2 8 19 -1
342.13 -19.69
342.13 19.68
OPEN   2 8 19 -1
322.44 -0   
361.81 -0   
T250   0     250   0     1
T-250  0     -250  0     2
PAD 1 3 P 62.99
-2 118.11 S   0  
-1 118.11 S   0  
0  118.11 S   0  
PAD 2 3 P 62.99
-2 118.11 R  
-1 118.11 R  
0  118.11 R  

*END*
