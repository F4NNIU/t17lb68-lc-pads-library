*PADS-LIBRARY-PCB-DECALS-V9*

LC-SOD-123       I 0     0     2 2 6 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-92.52 42.45 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-92.52 92.45 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 3.94 18 -1
-53.15 -32.48
-53.15 32.48
53.15 32.48
53.15 -32.48
-53.15 -32.48
CLOSED 5 7.87 26 -1
-19.68 27.56
19.68 27.56
19.68 -27.56
-19.68 -27.56
-19.68 27.56
OPEN   2 7.87 26 -1
-7.87 -27.56
-7.87 27.56
OPEN   2 3.94 19 -1
102.36 -11.81
102.36 11.81
OPEN   2 3.94 19 -1
90.55 0    
114.17 0    
OPEN   2 3.94 19 -1
-90.55 -11.81
-90.55 11.81
T58.07 0     58.07 0     1
T-58.07 0     -58.07 0     2
PAD 0 3 P 0    
-2 39.37 RF  0   0    49.21 0  
-1 0   R  
0  0   R  

*END*
