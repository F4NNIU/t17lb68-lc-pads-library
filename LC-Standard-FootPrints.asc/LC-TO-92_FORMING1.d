*PADS-LIBRARY-PCB-DECALS-V9*

LC-TO-92_FORMING1 I 0     0     2 2 3 0 3 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-123.62 110.8 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-123.62 160.8 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
OPEN   2 10 26 -1
80.11 56.09 350 1100 -97.8 -97.8 97.8  97.8 
-80.11 56.09
OPEN   3 3.94 18 -1
-77.95 -59.06 2171 -2542 -97.8 -97.8 97.8  97.8 
77.95 -59.06
-77.95 -59.05
OPEN   2 10 26 -1
-77.95 -59.05
77.95 -59.05
T-100  0     -100  0     1
T0     0     0     0     2
T100   0     100   0     3
PAD 0 3 P 31.5 
-2 47.24 OF  90   78.74 0  
-1 47.24 OF  90   78.74 0  
0  47.24 OF  90   78.74 0  
PAD 2 3 P 31.5 
-2 47.24 OF  90   78.74 0  
-1 47.24 OF  90   78.74 0  
0  47.24 OF  90   78.74 0  

*END*
