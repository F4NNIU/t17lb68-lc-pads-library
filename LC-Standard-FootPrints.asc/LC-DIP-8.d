*PADS-LIBRARY-PCB-DECALS-V9*

LC-DIP-8         I 0     0     2 2 3 0 8 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-179.53 220.2 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-179.53 270.2 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 11.81 26 -1
-143.7 200.39
-155.51 200.39
CLOSED 7 10 26 -1
-25   188.98 1800 1800 -25   163.98 25    213.98
25    188.98
104.33 188.98
104.33 -188.98
-104.33 -188.98
-104.33 188.98
-25   188.98
CIRCLE 2 23.62 26 -1
-53.15 149.61
-76.77 149.61
T-150  150   -150  150   1
T-150  50    -150  50    2
T-150  -50   -150  -50   3
T-150  -150  -150  -150  4
T150   -150  150   -150  5
T150   -50   150   -50   6
T150   50    150   50    7
T150   150   150   150   8
PAD 1 3 P 35.43
-2 59.06 S   0  
-1 59.06 S   0  
0  59.06 S   0  
PAD 0 3 P 35.43
-2 59.06 R  
-1 59.06 R  
0  59.06 R  

*END*
