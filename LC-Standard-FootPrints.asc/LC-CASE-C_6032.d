*PADS-LIBRARY-PCB-DECALS-V9*

LC-CASE-C_6032   I 0     0     2 2 6 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-181.17 85.83 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-181.17 135.83 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
OPEN   6 10 26 -1
55.12 -72.83
157.76 -72.83
175.98 -47.24
175.98 47.24
157.76 72.83
55.12 72.83
OPEN   2 10 26 -1
157.76 -72.83
157.76 72.83
OPEN   4 10 26 -1
-55.12 -72.83
-157.76 -72.83
-157.76 72.83
-55.12 72.83
OPEN   2 8 19 -1
208.66 -19.68
208.66 19.68
OPEN   2 8 19 -1
188.98 0    
228.35 0    
OPEN   2 8 19 -1
-177.17 -19.68
-177.17 19.68
T97.64 0     97.64 0     1
T-97.64 0     -97.64 0     2
PAD 0 3 P 0    
-2 94.49 RF  0   90   110.24 0  
-1 0   R  
0  0   R  

*END*
