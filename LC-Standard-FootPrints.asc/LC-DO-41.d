*PADS-LIBRARY-PCB-DECALS-V9*

LC-DO-41         I 0     0     2 2 7 0 2 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-259.91 72.06 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-259.91 122.06 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
OPEN   4 10 26 -1
-51.18 -55.12
-51.18 59.06
-43.31 55.12
-43.31 -59.06
OPEN   2 10 26 -1
78.74 0    
137.8 0    
CLOSED 5 10 26 -1
-78.74 -59.06
-78.74 59.06
78.74 59.06
78.74 -59.06
-78.74 -59.06
OPEN   2 10 26 -1
-137.8 0    
-78.74 0    
OPEN   2 8 19 -1
276.77 -19.68
276.77 19.68
OPEN   2 8 19 -1
257.09 0    
296.46 0    
OPEN   2 8 19 -1
-255.91 -19.68
-255.91 19.68
T200   0     200   0     1
T-200  0     -200  0     2
PAD 1 3 P 47.24
-2 86.61 S   0  
-1 86.61 S   0  
0  86.61 S   0  
PAD 2 3 P 47.24
-2 86.61 R  
-1 86.61 R  
0  86.61 R  

*END*
