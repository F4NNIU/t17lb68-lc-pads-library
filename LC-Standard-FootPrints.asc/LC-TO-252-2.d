*PADS-LIBRARY-PCB-DECALS-V9*

LC-TO-252-2      I 0     0     2 2 4 0 3 3 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-428.23 153.67 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-428.23 203.67 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 15.75 26 -1
-66.93 -129.92
-82.68 -129.92
OPEN   4 10 26 -1
-110.4 83.56
-59.22 83.56
-59.22 103.25
-110.4 103.25
CLOSED 5 10 26 -1
-423.23 -140.67
-423.23 140.67
-110.24 140.67
-110.24 -140.67
-423.23 -140.67
OPEN   4 10 26 -1
-110.24 -102.36
-59.06 -102.36
-59.06 -82.68
-110.24 -82.68
T0     -92.96 0     -92.96 1
T-275.59 -0    -275.59 -0    2
T-0.16 92.96 -0.16 92.96 3
PAD 1 3 P 0    
-2 47.24 RF  0   0    86.61 0  
-1 0   R  
0  0   R  
PAD 2 3 P 0    
-2 236.22 RF  0   0    255.9 0  
-1 0   R  
0  0   R  
PAD 3 3 P 0    
-2 47.24 RF  0   0    86.61 0  
-1 0   R  
0  0   R  

*END*
