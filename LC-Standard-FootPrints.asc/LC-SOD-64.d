*PADS-LIBRARY-PCB-DECALS-V9*

LC-SOD-64        I 0     0     2 2 7 0 2 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-263.84 99.61 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-263.84 149.61 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
OPEN   4 10 26 -1
-51.18 86.61
-51.18 -86.61
-59.05 -86.61
-59.05 86.61
OPEN   2 10 26 -1
-125.98 0    
-80   0    
OPEN   2 10 26 -1
80    0    
125.98 0    
CLOSED 5 10 26 -1
-80   -86.61
-80   86.61
80    86.61
80    -86.61
-80   -86.61
OPEN   2 8 19 -1
259.84 0    
299.21 0    
OPEN   2 8 19 -1
279.53 -19.68
279.53 19.68
OPEN   2 8 19 -1
-259.84 -19.68
-259.84 19.68
T196.85 0     196.85 0     1
T-196.85 0     -196.85 0     2
PAD 1 3 P 62.99
-2 98.43 RF  0   0    98.43 0  
-1 98.43 RF  0   0    98.43 0  
0  98.43 RF  0   0    98.43 0  
PAD 2 3 P 62.99
-2 98.43 R  
-1 98.43 R  
0  98.43 R  

*END*
