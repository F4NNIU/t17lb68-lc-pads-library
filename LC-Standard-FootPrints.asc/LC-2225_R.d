*PADS-LIBRARY-PCB-DECALS-V9*

LC-2225_R        I 0     0     2 2 3 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-148.43 156.03 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-148.43 206.03 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 3.94 18 -1
-112.2 124.02
112.2 124.02
112.2 -124.02
-112.2 -124.02
-112.2 124.02
OPEN   4 7.87 26 -1
-62.99 -144.09
-144.49 -144.09
-144.49 144.09
-62.99 144.09
OPEN   4 7.87 26 -1
62.99 -144.09
144.49 -144.09
144.49 144.09
62.99 144.09
T-103.15 0     -103.15 0     1
T103.15 0     103.15 0     2
PAD 0 3 P 0    
-2 59.06 RF  0   90   264.57 0  
-1 0   R  
0  0   R  

*END*
