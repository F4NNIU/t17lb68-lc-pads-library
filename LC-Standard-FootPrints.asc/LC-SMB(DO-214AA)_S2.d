*PADS-LIBRARY-PCB-DECALS-V9*

LC-SMB(DO-214AA)_S2 I 0     0     2 2 9 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-149.67 95.68 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-149.67 145.68 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 3.94 18 -1
89.96 -75  
89.96 75   
-89.96 75   
-89.96 -75  
89.96 -75  
CLOSED 4 10 26 -1
-19.68 0    
19.68 -39.37
19.68 39.37
-19.68 0    
OPEN   2 10 26 -1
-19.68 -39.37
-19.68 39.37
OPEN   2 10 26 -1
-31.5 0    
31.5  0    
OPEN   4 10 26 -1
98.43 -66.93
98.43 -82.68
-98.43 -82.68
-98.43 -66.93
OPEN   4 10 26 -1
98.43 66.93
98.43 82.68
-98.43 82.68
-98.43 66.93
OPEN   2 8 19 -1
-145.67 -19.68
-145.67 19.68
OPEN   2 8 19 -1
165.35 -19.68
165.35 19.68
OPEN   2 8 19 -1
145.67 0    
185.04 0    
T87.01 0     87.01 0     1
T-87.01 0     -87.01 0     2
PAD 0 3 P 0    
-2 85  RF  0   90   107.99 0  
-1 0   R  
0  0   R  

*END*
