*PADS-LIBRARY-PCB-DECALS-V9*

LC-SMD2018       I 0     0     2 2 4 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-143.78 124.22 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-143.78 174.22 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 3.94 18 -1
-106.3 -98.43
-106.3 98.43
106.3 98.43
106.3 -98.43
-106.3 -98.43
OPEN   4 10 26 -1
59.06 111.22
138.78 111.22
138.78 -111.22
59.06 -111.22
OPEN   4 10 26 -1
-59.06 111.22
-138.78 111.22
-138.78 -111.22
-59.06 -111.22
OPEN   4 10 26 -1
-51.18 -86.61
-51.18 -31.5
51.18 31.5 
51.18 86.61
T-96.46 0     -96.46 0     1
T96.46 0     96.46 0     2
PAD 0 3 P 0    
-2 59.06 RF  0   90   196.85 0  
-1 0   R  
0  0   R  

*END*
