*PADS-LIBRARY-PCB-DECALS-V9*

LC-SOT-89(SOT-89-3) I 0     0     2 2 3 0 4 3 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-167.32 103.55 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-167.32 153.55 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
OPEN   3 10 26 -1
-19.68 -90.55
-122.05 -90.55
-122.05 -55.12
OPEN   3 10 26 -1
-19.68 90.55
-122.05 90.55
-122.05 55.12
CLOSED 5 3.94 18 -1
-19.88 -90.55
-19.88 90.55
-121.85 90.55
-121.85 -90.55
-19.88 -90.55
T0     -59.06 0     -59.06 1
T-3.94 0     -3.94 0     2
T-98.43 0     -98.43 0     2/2
T0     59.06 0     59.06 3
PAD 0 3 P 0    
-2 27.56 OF  0    74.8 0  
-1 0   R  
0  0   R  
PAD 2 3 P 0    
-2 27.56 OF  0    82.68 0  
-1 0   R  
0  0   R  
PAD 3 3 P 0    
-2 78.74 OF  0    137.8 0  
-1 0   R  
0  0   R  

*END*
