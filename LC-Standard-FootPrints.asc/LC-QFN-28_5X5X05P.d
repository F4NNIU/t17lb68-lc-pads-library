*PADS-LIBRARY-PCB-DECALS-V9*

LC-QFN-28_5X5X05P I 0     0     2 2 7 0 29 16 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-114.2 122.2 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-114.2 172.2 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 11.81 26 -1
-90.55 -124.02
-102.36 -124.02
CIRCLE 2 3.94 18 -1
-62.99 -74.8
-86.61 -74.8
OPEN   3 7.87 26 -1
-78.74 -104.33
-104.33 -104.33
-104.33 -78.74
OPEN   3 7.87 26 -1
-104.33 78.74
-104.33 104.33
-78.74 104.33
OPEN   3 7.87 26 -1
78.74 104.33
104.33 104.33
104.33 78.74
OPEN   3 7.87 26 -1
78.74 -104.33
104.33 -104.33
104.33 -78.74
CLOSED 5 3.94 18 -1
-98.43 -98.43
98.43 -98.43
98.43 98.43
-98.43 98.43
-98.43 -98.43
T0     0     0     0     0
T-59.06 -95.47 -59.06 -95.47 1
T-39.37 -95.47 -39.37 -95.47 2
T-19.68 -95.47 -19.68 -95.47 3
T0     -95.47 0     -95.47 4
T19.69 -95.47 19.69 -95.47 5
T39.37 -95.47 39.37 -95.47 6
T59.06 -95.47 59.06 -95.47 7
T95.47 -59.06 95.47 -59.06 8
T95.47 -39.37 95.47 -39.37 9
T95.47 -19.68 95.47 -19.68 10
T95.47 0     95.47 0     11
T95.47 19.69 95.47 19.69 12
T95.47 39.37 95.47 39.37 13
T95.47 59.06 95.47 59.06 14
T59.06 95.47 59.06 95.47 15
T39.37 95.47 39.37 95.47 16
T19.69 95.47 19.69 95.47 17
T0     95.47 0     95.47 18
T-19.68 95.47 -19.68 95.47 19
T-39.37 95.47 -39.37 95.47 20
T-59.06 95.47 -59.06 95.47 21
T-95.47 59.06 -95.47 59.06 22
T-95.47 39.37 -95.47 39.37 23
T-95.47 19.69 -95.47 19.69 24
T-95.47 0     -95.47 0     25
T-95.47 -19.68 -95.47 -19.68 26
T-95.47 -39.37 -95.47 -39.37 27
T-95.47 -59.06 -95.47 -59.06 28
PAD 1 4 P 0    
-2 123.23 RF  0   90   123.23 0  
-1 0   R  
0  0   R  
21 127.23 RF  0   90   127.23 0  
PAD 0 4 P 0    
-2 11.02 OF  90   33.46 0  
-1 0   R  
0  0   R  
21 15.02 OF  90   37.46 0  
PAD 9 4 P 0    
-2 11.02 OF  0    33.46 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    37.46 0  
PAD 10 4 P 0    
-2 11.02 OF  0    33.46 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    37.46 0  
PAD 11 4 P 0    
-2 11.02 OF  0    33.46 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    37.46 0  
PAD 12 4 P 0    
-2 11.02 OF  0    33.46 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    37.46 0  
PAD 13 4 P 0    
-2 11.02 OF  0    33.46 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    37.46 0  
PAD 14 4 P 0    
-2 11.02 OF  0    33.46 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    37.46 0  
PAD 15 4 P 0    
-2 11.02 OF  0    33.46 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    37.46 0  
PAD 23 4 P 0    
-2 11.02 OF  0    33.46 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    37.46 0  
PAD 24 4 P 0    
-2 11.02 OF  0    33.46 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    37.46 0  
PAD 25 4 P 0    
-2 11.02 OF  0    33.46 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    37.46 0  
PAD 26 4 P 0    
-2 11.02 OF  0    33.46 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    37.46 0  
PAD 27 4 P 0    
-2 11.02 OF  0    33.46 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    37.46 0  
PAD 28 4 P 0    
-2 11.02 OF  0    33.46 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    37.46 0  
PAD 29 4 P 0    
-2 11.02 OF  0    33.46 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    37.46 0  

*END*
